/*
This file is part of jpath.

jpath is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

jpath is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with jpath.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <cajun/json/elements.h>
#include <cajun/json/reader.h>
#include <cajun/json/writer.h>
using namespace std;

#include "util.h"

const char help [] = R"(
jpath 1.0
Copyright Techneaux Technologies, LLC.
License GPLv3+
Written by Chris Parich

Usage:
Returns the JSON-encoded value of the specified path.

$ echo '{ "key":{ "key":"value" } }' | jpath key/key
"value"
$ echo '{ "key":{ "key":"value" } }' | jpath key
{
	"key":"value"
}
$ echo '{"key":{"key":["value"] } }' | jpath key/key/0
"value"

$ jpath key/key document.json
"value"

Strings are always enclosed in quotes, so shell removal may be required.
)";

int main(int argc, char** argv) {

	json::Object obj;
	json::UnknownElement unk;

	if (argc < 2 || string(argv[1]) == "-h" || string(argv[1]) == "--help") {
		cerr << help << endl;
		return 0;
	}

	try {
		if (argc == 2)
			json::Reader::Read(obj, cin);
		else if (argc == 3) {
			ifstream file(argv[2]);
			json::Reader::Read(obj, file);
		}
	} catch (...) {
		cerr << "Could not read input" << endl;
		exit(EXIT_FAILURE);
	}
	unk = obj;

	auto tokens = tokenize(argv[1], "/");

	for (string token : tokens) {

		bool treat_as_array = false;

		long index = -1;
		try {
			index = stol(token);
			treat_as_array = true;
		} catch (...) {
			treat_as_array = false;
		}


		if (treat_as_array && index == -1) {
			cerr << "Could not read " << token << " as part of path" << endl;
			exit(EXIT_FAILURE);
		}
		try {
			unk = treat_as_array ? json::Array(unk)[index] : json::Object(unk)[token];
		} catch (...) {
			cerr << "Could not read path at " << token << endl;
			exit(EXIT_FAILURE);
		}
	}

	json::Writer::Write(unk, cout);

	return 0;
}
